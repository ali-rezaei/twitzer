package com.dynamo.android.client.common.view;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class HidingScrollListener extends RecyclerView.OnScrollListener {

    private static final int HIDE_THRESHOLD = 20;

    private int mScrolledDistance = 0;
    private boolean mControlVisible;

    public HidingScrollListener(boolean isToolbarVisible) {
        this.mControlVisible = isToolbarVisible;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

        if (firstVisibleItem == 0) {
            if(!mControlVisible) {
                onShow();
                mControlVisible = true;
            }
        } else {
            if (mScrolledDistance > HIDE_THRESHOLD && mControlVisible) {
                onHide();
                mControlVisible = false;
                mScrolledDistance = 0;
            } else if (mScrolledDistance < -HIDE_THRESHOLD && !mControlVisible) {
                onShow();
                mControlVisible = true;
                mScrolledDistance = 0;
            }
        }
        if((mControlVisible && dy>0) || (!mControlVisible && dy<0)) {
            mScrolledDistance += dy;
        }
    }

    public abstract void onHide();
    public abstract void onShow();
}
