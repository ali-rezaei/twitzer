package com.dynamo.android.client;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class TwitzerApplication extends Application {

    private static Context mCtx;

    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        mCtx = this.getApplicationContext();
    }

    public static Context getContext() {
        return mCtx;
    }
}
