package com.dynamo.android.client.model;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import com.dynamo.android.client.R;
import com.dynamo.android.client.TwitzerApplication;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Twitz implements Parcelable {

    private String url;
    private String text;
    private String date;
    private String user;
    private String id;
    private String img;
    private String name;

    public static final Parcelable.Creator<Twitz> CREATOR = new Parcelable.Creator<Twitz>() {

        @Override
        public Twitz createFromParcel(Parcel source) {
            return new Twitz(source);
        }

        @Override
        public Twitz[] newArray(int size) {
            return new Twitz[size];
        }
    };

    private Twitz(Parcel in) {
        this.url = in.readString();
        this.text = in.readString();
        this.date = in.readString();
        this.user = in.readString();
        this.id = in.readString();
        this.img = in.readString();
        this.name = in.readString();
    }

    public String getUrl() {
        return url;
    }

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }

    public String getUser() {
        return user;
    }

    public String getId() {
        return id;
    }

    public String getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(text);
        dest.writeString(date);
        dest.writeString(user);
        dest.writeString(id);
        dest.writeString(img);
        dest.writeString(name);
    }

    public SpannableString getFormatedTwitzText() {
        if (text.indexOf(" lang=") == 0) {
            text = text.split(" lang=\".*\" data-aria-label-part=\".*\">", 2)[1];
        }
        SpannableString spannableString = new SpannableString(text);
        Matcher matcher = Pattern.compile("#([A-Za-z0-9_-]+)").matcher(spannableString);
        Context context = TwitzerApplication.getContext();
        while (matcher.find()) {
            spannableString.setSpan(new ForegroundColorSpan(
                            context.getResources().getColor(R.color.hash_tag_color)),
                    matcher.start(),
                    matcher.end(),
                    0);

        }
        return spannableString;
    }
}
