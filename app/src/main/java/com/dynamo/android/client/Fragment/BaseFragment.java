package com.dynamo.android.client.Fragment;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

public abstract class BaseFragment extends Fragment {

    protected class TwitzErrorHandler implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            // Handle your error types accordingly.For Timeout & No connection error, you can show 'retry' button.
            // For AuthFailure, you can re login with user credentials.
            // For ClientError, 400 & 401, Errors happening on client side when sending api request.
            // In this case you can check how client is forming the api and debug accordingly.
            // For ServerError 5xx, you can do retry or handle accordingly.
            if (error instanceof NetworkError) {
                showToast(error.getMessage());
            } else if( error instanceof AuthFailureError) {
                showToast(error.getMessage());
            } else if( error instanceof ParseError) {
                showToast(error.getMessage());
            } else if( error instanceof TimeoutError) {
                showToast(error.getMessage());
            } else if( error instanceof ServerError) {
                showToast(error.getMessage());
            }
        }
    }

    protected void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }
}
